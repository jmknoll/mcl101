class StaticPagesController < ApplicationController
  def home
  end

  def lessons
  end

  def about
  end

  def contact
  end
end
