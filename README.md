==This is the source code for Mandarin Chinese Lessons 101, an experimental project in skill development, product development, and foriegn language education.

While studying Chinese, I set out to build a comprehensive set of phrases which, taken together, could help propel a student of Chinese to an intermediate conversation level 
with much less work than is required with most other systems of learning the language. My rationale for this was that my native tongue English is so
syntactically different from Chinese that communication can be difficult even with a relatively advanced vocabulary. 

This project grew over time. What began as a word document later became an experiment in teaching myself some web development, as I decided that I wanted to turn
the project into an interactive web application. 

It then continued to grow as I decided that I would also use this opportunity to start teaching myself some business skills like project management and marketing.

Although I would, of course, prefer that people purchase a membership to the site (its only $5!), my main purpose in this adventure was not to make money, 
but rather just to have a good reason to teach myself some useful skills. Hence it being freely available on github.

Best,
Jameson
